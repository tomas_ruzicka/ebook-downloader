import re
import wget
import time
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

book_base_url = 'https://www.databazeknih.cz/'
url = 'https://www.databazeknih.cz/novinky/vice-nez-100-dalsich-eknih-zdarma-1062?fbclid=IwAR0cWJn3hbbOyfIFK60D2I_ucrrpMQZD_wi12HAgE44HxuLbFzavXggy5bQ'

def simple_get(url):
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None

def is_good_response(resp):
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 and content_type is not None and content_type.find('html') > -1)

def log_error(e):
    print(e)


base_raw_html = simple_get(url)
base_html = BeautifulSoup(base_raw_html, 'html.parser')

links = base_html.find_all("a", class_="biggest odr")

index = 1
for link in links:
	book_target_url = book_base_url + link['href']
	book_raw_html = simple_get(book_target_url)
	book_html = BeautifulSoup(book_raw_html, 'html.parser')
	book_link = book_html.find_all("a", href=re.compile("https://web2.mlp.cz/*"))
	if len(book_link) <= 0:
		continue
	book_download_element = book_link[0] # by default select first element (index [0])
	if book_download_element['href'] == None:
		continue

	book_download_link = book_download_element['href'] 

	print('[{0}]'.format(index))
	print('Downloading: {0}'.format(book_download_link))
	file_name = wget.download(book_download_link)
	print('Downloaded file: {0}'.format(file_name))
	index += 1
	time.sleep(5) # waiting 5 seconds

